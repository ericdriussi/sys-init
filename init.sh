#!/usr/bin/env bash

set -o nounset
set -o errexit
set -o pipefail

rm -rf ~/Documents/sys-init && git clone git@gitlab.com:ericdriussi/sys-init.git ~/Documents/sys-init

if command -v apt &>/dev/null; then
  install_cmd="apt install -y pipx"
elif command -v dnf &>/dev/null; then
  install_cmd="dnf install -y pipx"
elif command -v pacman &>/dev/null; then
  install_cmd="pacman --noconfirm -S python-pipx"
else
  echo "Unsupported package manager."
  exit 1
fi

if [ "$EUID" -eq 0 ]; then # is root user
  eval "$install_cmd"
else
  sudo cp -n ~/.ssh/* /root/.ssh/
  eval sudo "$install_cmd"
fi

pipx install --include-deps ansible

cd ~/Documents/sys-init/
~/.local/bin/ansible-playbook run.yml --ask-become-pass "$@"
