# Sys-init

> Simple script to auto-init my system(s)

- Add pre-authorized ssh key pair for gitlab/github to `~/.ssh/`.
- Run `curl -sSL https://gitlab.com/ericdriussi/sys-init/-/raw/master/init.sh | bash`
